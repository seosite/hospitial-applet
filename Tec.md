## 微信小程序部分说明文档
##### Author [Earl](http://www.seosite.cn) guo@seosite.cn 
#### 1、let
    
       xx function(e){
       
            let This = this;
       
       }
这里的 *let* 是做方法内的变量申明,方法外即失效。

#### 2、百度地图插件

 
 - [官方教程](http://lbsyun.baidu.com/index.php?title=wxjsapi)
 
    在百度地图开放平台中创建小程序应用，需要填入小程序的APPID
    
 - [下载地址](https://github.com/baidumapapi/wxapp-jsapi)


 - 使用方法

    - 把下载好的JS插件存储在根目录下的libs文件夹下
    - 在使用的时候需要引用这个文件，具体的方法是；
    
        `var bmap = require ('../../libs/bmap-wx.min.js');`
        
    - 在使用的时候，由于bmap.js是一个类
        
        `var BMap = new bmap.BMapWX({
            ak: 'your baidu-weixins appkey!'
        });
        //获取定位，设置用户位置
        BMap.regeocoding({
            success:function (e) {
                console.log('auth success:');
                console.log(e);
                },
            fail:function (){
                console.log('auth deny');
            }
        });`
    
 
#### 3、添加分享

    Page({
        onShareAppMessage: function () {
            return {
                title: '合肥产科医疗床位查询系统',
                desc: '最快速的查找你想要',
                path: '/page/index'
            }
        }
    })
    
    
#### 4、获取用户openid

    wx.login({
        success: function (e) {
            wx.request({
                url: 'https://api.ahlife.com/applet/?service=WechatMini_WXLoginHelper.checkLogin',
                data: {code: e.code},
                header: {
                    'content-type': 'application/json'
                    },
                success: function (res) {
                    var data = res.data;
                    if (data.ret == 200 && data.data.code == 0) {
                        console.log("登录成功");
                            console.log("openid = " + data.data.openid);
                            console.log("session3rd = " + data.data.session3rd);
                            wx.setStorageSync('session3rd',data.data.session3rd);
                            wx.setStorageSync('openID',data.data.openid);
                        } else {
                            console.log("登录失败");
                            console.log("code = " + data.data.code);
                            console.log("message = " + data.data.message)
                        }
                    },
                fail: function (res) {
                    console.log(res);
                }
            })
        }
    })          
    
    
   `wx.login({})` 调用微信登录的一个函数，可以返回登录凭证`code`,进而换取用户的登录信息，返回openid，绘画密钥session_key。`wx.login` 
   进行授权登录不需要用户同意，属于静默授权，成功返回`code`。
   
   
   `code` 获取openid，接口地址`https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code`
   
   请求的参数是：
   `appid` 小程序的唯一标识,`secret` 小程序的密钥 ,`js_code` 返回的 code,`grant_type` 默认填写为`authorization_code`
   
   返回值是：
   openid,sesion3rd
   
#### 5、用户缓存

- 官方文档

    - `https://mp.weixin.qq.com/debug/wxadoc/dev/api/data.html#wxgetstorageinfoobject`


- 异步缓存和同步缓存的区别

    异步缓存不会阻塞当前任务，同步缓存直到同步方法处理完才能袖往下执行。
    
- 异步缓存

    - 设置缓存 wx.setStorage(OBJ) 
    
        `wx.setStorage({key:'key',data:'data',success:function(){}})`
    
    - 获取缓存 wx.getStorage(OBJ) 
    
        `wx.getStorage({key:'key',success:function(){}})`
    
    - 移除缓存 wx.removeStorage(OBJ)
    
        `wx.removeStorage({key:'key',success:function(){}})`
    
    - 清空缓存 wx.clearStorage()
    
        `wx.clearStorage()`
        
    - 获取当前storage 的相关信息 wx.getStorageInfo(Obj)
        
        `wx.getStorageInfo({success:function(){}})`
        
        返回值：
        
        keys arr 当前storage中的所有的key
        
        currentSize 当前占用的空间大小，单位kb
        
        limitSize  限制的空间大小，单位kb
    
    
- 同步缓存

    - 设置缓存 wx.setStorageSync(key,value) 
        
    - 获取缓存 wx.getStorageSync(key) 
        
    - 移除缓存 wx.removeStorageSync(key)
        
    - 清空缓存 wx.clearStorageSync()
    
    - 获取当前storage 的相关信息 wx.getStorageInfoSync(Obj)
            
        `wx.getStorageInfoSync({success:function(){}})`
            
        返回值：
            
        keys arr 当前storage中的所有的key
            
        currentSize 当前占用的空间大小，单位kb
            
        limitSize  限制的空间大小，单位kb  


   
   
   
   
   
   