## 医疗微信小程序 
###### Author [郭毅](http://www.seosite.cn) guo@seosite.cn

- 小程序更新说明按时间顺序
    - 技术文档请见tec.md

- 2017.7.5

    - 添加百度地图插件
    - 首页列表

- 2017.7.6
    
    - 搜索、留言、详情页面
    - 添加分享、转发
    - 添加同步缓存
    - 添加获取openid
