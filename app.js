//app.js
App({
    onLaunch: function () {
        //调用API从本地缓存中获取数据
        var logs = wx.getStorageSync('logs') || [];
        logs.unshift(Date.now());
        wx.setStorageSync('logs', logs);
        this.getUserToStorage();//静默授权，不需要获取用户信息，可以获取到用户到openid
        this.getUserInfo();//需要用户同意授权，才可以获取到用户到个人信息
    },
    getUserToStorage: function (cb) {
        console.log('调用了getUserToStorage的接口');
        var that = this;
        if (wx.getStorageSync('session3rd') == '') {
            //调用登录接口
            wx.login({
                success: function (e) {
                    wx.request({
                        url: 'https://api.ahlife.com/applet/?service=WechatMini_WXLoginHelper.checkLogin',
                        data: {code: e.code},
                        header: {
                            'content-type': 'application/json'
                        },
                        success: function (res) {
                            var data = res.data;
                            if (data.ret == 200 && data.data.code == 0) {
                                console.log("登录成功");
                                console.log("openid = " + data.data.openid);
                                console.log("session3rd = " + data.data.session3rd);
                                wx.setStorageSync('session3rd',data.data.session3rd);
                                wx.setStorageSync('openID',data.data.openid);
                            } else {
                                console.log("登录失败");
                                console.log("code = " + data.data.code);
                                console.log("message = " + data.data.message)
                            }
                        },
                        fail: function (res) {
                            console.log(res);
                        }
                    })
                }
            })
        }
    },
    getUserInfo: function (cb) {
        var that = this;
        if (this.globalData.userInfo) {
            typeof cb == "function" && cb(this.globalData.userInfo)
        } else {
            //调用登录接口
            wx.login({
                success: function (res) {
                    wx.getUserInfo({
                        success: function (res) {
                            that.globalData.userInfo = res.userInfo;
                            wx.setStorageSync('userInfo',res.userInfo);
                            typeof cb == "function" && cb(that.globalData.userInfo)
                        }
                    })
                }
            })
        }
    },
    globalData: {
    }
});