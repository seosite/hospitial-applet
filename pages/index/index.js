// pages/index/index.js
var bmap = require ('../../libs/bmap-wx.min.js');
Page({
    //分享转发按钮设置
    onShareAppMessage: function () {
        return {
            title: '合肥产科医疗床位查询系统',
            desc: '最快速的查找你想要',
            path: '/page/index'
        }
    },
    data: {
        listcur: '',
        address: '',
        latitude:'',
        longitude:'',
        ifcan: true,
        dataType: 'all',//all,distance,rank,bed,keyword
        pageNum: '1',
        ifLastPage: false,
        showLastHide: 'showLastHide',
        listArr: [],
        ss_hide: 'ss_hide',
        if_ss: false,
        ss_key: '',
        no_data_hide: 'no_data_hide',
        rm: [],
        ls: [],
    },
    //显示搜索界面
    ssShow: function () {
        let This = this;
        if (This.data.ss_key) {
            This.setData({
                if_ss: true
            });
        } else {
            This.setData({
                if_ss: false
            });
        }

        //搜索历史
        wx.request({
            url: 'https://api.ahlife.com/applet/?service=hospital.searchHistory',
            data: {
                uid:wx.getStorageSync('openID')
            },
            header: {
                'content-type': 'application/json'
            },
            success: function (e) {
                This.setData({
                    ls:e.data.data.info
                })

            },
            fail:function (e) {
                console.log(e);
            }
        });
        This.setData({
            ss_hide: ''
        });

        //热门搜索
        wx.request({
            url: 'https://api.ahlife.com/applet/?service=hospital.hotSearch',
            header: {
                'content-type': 'application/json'
            },
            success: function (e) {
                This.setData({
                    rm:e.data.data.info
                })

            },
            fail:function (e) {
                console.log(e);
            }
        });
        This.setData({
            ss_hide: ''
        });
    },
    //隐藏搜索界面
    ssHide: function () {
        let This = this;
        if (!This.data.if_ss) {
            This.setData({
                ss_hide: 'ss_hide',
                ss_key: ''
            });
        } else {
            This.setData({
                pageNum: '1',
                ss_key: ''
            });
            //搜索界面取消时获取全部列表
            This.getArr();
        }

    },
    //输入内容时设置搜索内容
    setInput: function (e) {
        let This = this;
        This.setData({
            ss_key: e.detail.value
        });
    },
    //提交并搜索
    sendSs: function (e) {
        let This = this;
        if (e.currentTarget.dataset.key) {
            This.setData({
                pageNum: 1,
                dataType:'keyword',
                ss_key: e.currentTarget.dataset.key
            });
        } else {
            if (This.data.ss_key.replace(/\s/ig, "") == '') {
                wx.showModal({
                    title: '提示',
                    content: '请输入搜索内容',
                    showCancel: false
                });
                return;
            }
            This.setData({
                pageNum: 1,
                dataType:'keyword'
            });
        }

        //搜索关键字获取列表(dataType 是显示搜索界面前的类别)
        This.getArr();
    },
    //选择底部筛选项
    changeData: function (e) {
        let This = this;
        This.setData({
            listcur: e.currentTarget.dataset.type,
            dataType: e.currentTarget.dataset.type,
            pageNum: 1
        });
        //选择底部筛选项加载获取列表
        This.getArr();
    },
    myFun:function (e) {
        let This = this;
        console.log(This.data.my_data)
    },
    showDeny:function (e) {
        let This = this;
        if(This.data.address == '授权被拒绝...'){
            wx.showModal({
                title:'解决方法',
                content:'依次点击右上角关于合肥房产-右上角设置-打开地理位置'
            })
        }
    },
    //滚动至底部（加载下一页）
    loadMoreArr: function (e) {
        let This = this;
        if (This.data.ifLastPage) {
            This.setData({
                showLastHide: ''
            });
            return;//加载到最后一页里，滚动到底部时不再加载更多列表，并提示已到页面最底部
        }
        This.getArr();
    },
    onLoad: function (options) {
        let This = this;
        var BMap = new bmap.BMapWX({
            ak: 'a8T66pkYVINiGXUemGMVxPBITX7fLUct'
        });

        var session3rd = wx.getStorageSync('session3rd');
        var openID = wx.getStorageSync('openID');
        var userInfo = wx.getStorageSync('userInfo');

        console.log(session3rd);
        console.log(openID);
        console.log(userInfo);
        //获取定位，设置用户位置
        BMap.regeocoding({
            success:function (e) {
                console.log('auth success:');
                console.log(e);
                var wxMarkerData = e.wxMarkerData;
                This.setData({
                    address: wxMarkerData[0].address,
                    latitude: wxMarkerData[0].latitude,
                    longitude: wxMarkerData[0].longitude
                });
                This.getArr();
            },
            fail:function (){
                console.log('auth fail');
                This.setData({
                    address:'授权被拒绝...',
                    latitude:0,
                    longitude:0
                });
                This.getArr();
            }
        });
        //页面加载时获取全部列表

    },
    onReady: function () {
        // 页面渲染完成
    },
    onShow: function () {
        // 页面显示
    },
    onHide: function () {
        // 页面隐藏
    },
    onUnload: function () {
        // 页面关闭
    },
    //**************************获取列表 公共方法**************************//
    getArr: function () {
        let This = this;
        let dataType = This.data.dataType;
        let pageNum = This.data.pageNum;
        let dataKye = This.data.ss_key;
        let lon = This.data.longitude;
        let lat = This.data.latitude;

        wx.showLoading({
            title: '加载中...',
            mask: true
        });
        console.log('现在的地址是：'+This.data.address);
        console.log('现在的latitude是：'+This.data.latitude);
        console.log('现在的longitude是：'+This.data.longitude);

        if (This.data.ifcan) {
            This.setData({
                ifcan: false
            });
            //console.log(This.data.ifcan);
        } else {
            //console.log(This.data.ifcan);
            return;
        }
        console.log('排序类别=' + dataType, '页码=' + pageNum, '搜索关键词=' + dataKye);

        let ifLastPage;//当前加载的是否最后一页,true 是最后一页；false 不是最后一页
        let newArr;
        let isAjax = true;

        if (isAjax){
            isAjax = false;
            wx.request({
                url: 'https://api.ahlife.com/applet/?service=hospital.getHospitalByOrder',
                data: {
                    order: dataType,
                    pageNum: pageNum,
                    value: dataKye,
                    lon:lon,
                    lat:lat
                },
                header: {
                    'content-type': 'application/json'
                },
                success: function (e) {
                    console.log(e);
                    var res= e.data;
                    var dataList = res.data.info.list;

                    // return;
                    wx.hideLoading();
                    if (res.ret !=200){
                        wx.showModal({
                            'title':'提示',
                            'content':res.msg
                        });
                        This.setData({
                            no_data_hide: ''
                        });
                        isAjax = true;
                        return false;
                    }
                    if (!dataList.length) {// !res.listData.length  没有列表的内容时显示提示界面
                        This.setData({
                            no_data_hide: ''
                        });
                        isAjax = true;
                    } else {
                        ifLastPage = res.data.info.ifLastPage;
                        if (pageNum == 1) {
                            newArr = [];
                        } else {
                            newArr = This.data.listArr;
                        }
                        for (let i = 0; i < dataList.length; i++) {
                            newArr.push(dataList[i]);
                        }
                        //设置列表数据
                        This.setData({
                            ifcan: true,
                            pageNum: parseInt(pageNum) + 1,
                            listArr: newArr,
                            ss_hide: 'ss_hide',
                            ifLastPage: ifLastPage,
                            no_data_hide: 'no_data_hide'
                        });
                        isAjax = true;
                    }
                },
                fail:function (e) {
                    isAjax = true;
                    wx.hideLoading();
                    wx.showModalDialog({
                        'title':'提示',
                        'content':'网络错误，下拉重试'
                    })
                }
            })
        }


    }
});